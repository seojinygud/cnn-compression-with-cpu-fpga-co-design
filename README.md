# CNN Compression with CPU-FPGA Co-design

## Introduction

In this project, we propose a method that provides both the speed of CNNs and the performance and parallelism of FPGAs. This solution relies on two basic acceleration techniques: parallel processing of CNN layer resources and pipelining within specific layers. It also uses software-defined system-on-chip tools to implement automatic parallel hardware-software co-design CNNs, resulting in speed and design time benefits. We used 5 networks of MobileNetV1, ShuffleNetV2, SqueezeNet, ResNet-50, VGG-16 and FPGA processor ZCU102.

This work was supported by the Korea Evaluation Institute of Industrial Technology (KEIT). This makes it difficult to release the code right away. I will come soon.